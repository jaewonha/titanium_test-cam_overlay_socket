var win = Titanium.UI.createWindow();

var alphaPlane = Titanium.UI.createView({
	width : Titanium.UI.FULL,
	height : Titanium.UI.FULL,
	backgroundColor : '#88ff0000',
});

var scanner = Titanium.UI.createView({
	width : 260,
	height : 200,
	borderColor : 'red',
	borderWidth : 5,
	borderRadius : 15
});

var button = Titanium.UI.createButton({
	color : '#fff',
	//backgroundImage : '../images/BUTT_grn_on.png',
	//backgroundSelectedImage : '../images/BUTT_grn_off.png',
	//backgroundDisabledImage : '../images/BUTT_gry_on.png',
	bottom : 10,
	width : 301,
	height : 57,
	font : {
		fontSize : 20,
		fontWeight : 'bold',
		fontFamily : 'Helvetica Neue'
	},
	title : 'Take Picture'
});

var messageView = Titanium.UI.createView({
	height : 30,
	width : 250,
	visible : false
});

var indView = Titanium.UI.createView({
	height : 30,
	width : 250,
	backgroundColor : '#000',
	borderRadius : 10,
	opacity : 0.7
});
messageView.add(indView);

// message
var message = Titanium.UI.createLabel({
	text : 'Picture Taken',
	color : '#fff',
	font : {
		fontSize : 20,
		fontWeight : 'bold',
		fontFamily : 'Helvetica Neue'
	},
	width : 'auto',
	height : 'auto'
});
messageView.add(message);

var overlay = Titanium.UI.createView();
overlay.add(alphaPlane);
overlay.add(button);
overlay.add(messageView);

button.addEventListener('click', function() {
	alphaPlane.borderColor = '#ffff0000';
	Ti.Media.takePicture();
	messageView.animate({
		visible : true
	});
	setTimeout(function() {
		scanner.borderColor = '#ff0000ff';
		messageView.animate({
			visible : false
		});
	}, 500);
});

Titanium.Media.showCamera({
	saveToPhotoGallery : true,
	success : function(event) {
		/*
		 // place our picture into our window
		 var imageView = Ti.UI.createImageView({
		 image : event.media,
		 width : 640,
		 height : 480
		 });
		 win.add(imageView);

		 win.open();
		 */
		var body = event.media;
		body = body.imageAsResized(640, 480);

		//read image
		var bodyStream = Titanium.Stream.createStream({
			mode : Titanium.Stream.MODE_READ,
			source : body
		});

		var bodyBuffer = Ti.createBuffer({
			length : body.length
		});

		while ((bodyStream.read(bodyBuffer)) > 0) {
		}
		bodyStream.close();

		//write image
		var response = Ti.createBuffer({
			length : 1024
		});
		var stream = Ti.Stream.createStream({
			mode : Ti.Stream.MODE_WRITE,
			source : response
		});

		stream.write(bodyBuffer);
		//socket

		var recvRecogInfo = [];

		var TYPE_STR = 0;
		var TYPE_PIC = 1;

		var readBuffer = Ti.createBuffer();

		var socket = Ti.Network.Socket.createTCP({
			host : '125.132.41.25',
			//host : '172.25.235.46',
			port : 27016,
			connected : function(e) {
				Ti.API.info('Socket opened!');

				var header0 = Ti.createBuffer({
					value : 1,
					type : Ti.Codec.TYPE_INT,
					byteOrder : Ti.Codec.LITTLE_ENDIAN
				});
				var header1 = Ti.createBuffer({
					value : TYPE_PIC,
					type : Ti.Codec.TYPE_INT,
					byteOrder : Ti.Codec.LITTLE_ENDIAN
				});
				var header2 = Ti.createBuffer({
					value : bodyBuffer.length,
					type : Ti.Codec.TYPE_INT,
					byteOrder : Ti.Codec.LITTLE_ENDIAN
				});
				var header_id = Ti.createBuffer({
					value : genRandomId(),
					type : Ti.Codec.CHARSET_UTF8,
					byteOrder : Ti.Codec.LITTLE_ENDIAN,
					length : 24
				});

				var bufferIntegrated = Ti.createBuffer();

				bufferIntegrated.append(header0);
				bufferIntegrated.append(header1);
				bufferIntegrated.append(header2);
				bufferIntegrated.append(header_id);
				bufferIntegrated.append(bodyBuffer);

				Ti.Stream.pump(socket, readCallback, 4096, true);

				//Ti.Stream.write(socket, header0, writeCallback);
				//Ti.Stream.write(socket, header1, writeCallback);
				//Ti.Stream.write(socket, header2, writeCallback);
				//Ti.Stream.write(socket, header_id, writeCallback);

				Ti.Stream.write(socket, bufferIntegrated, writeCallback);
			},
			error : function(e) {
				Ti.API.info('Error (' + e.errorCode + '): ' + e.error);
			},
		});
		socket.connect();

		//alert("picture was taken");
		//alert(event);
		// programatically hide the camera
		//Ti.Media.hideCamera();

		function readCallback(e) {
			if (e.bytesProcessed == -1) {
				// Error / EOF on socket. Do any cleanup here.
				Ti.API.info("EOF on socket.");
				Ti.API.info("readBuffer.length: " + readBuffer.length);
				var bufferOffset = 0;
				Ti.API.info("initial bufferOffset: " + bufferOffset);
				var numPics = Ti.Codec.decodeNumber({
					source : readBuffer,
					position : bufferOffset,
					type : Ti.Codec.TYPE_INT,
					byteOrder : Ti.Codec.LITTLE_ENDIAN
				});
				bufferOffset += 4;

				Ti.API.info("onPictureTaken:numPics: " + numPics);

				for (var i = 0; i < numPics; i++) {

					Ti.API.info("bufferOffset before decoding type: " + bufferOffset);
					var typeReceived = Ti.Codec.decodeNumber({
						source : readBuffer,
						position : bufferOffset,
						type : Ti.Codec.TYPE_INT,
						byteOrder : Ti.Codec.LITTLE_ENDIAN
					});
					bufferOffset += 4;

					Ti.API.info("bufferOffset before decoding len: " + bufferOffset);
					var lenReceived = Ti.Codec.decodeNumber({
						source : readBuffer,
						position : bufferOffset,
						type : Ti.Codec.TYPE_INT,
						byteOrder : Ti.Codec.LITTLE_ENDIAN
					});
					bufferOffset += 4;

					Ti.API.info("recv:type: " + typeReceived + "/len: " + lenReceived);

					if (lenReceived > 1024 * 1024) {
						Ti.API.error("too large size: " + lenReceived);
						e.source.close();
					} else {
						var buffer = Ti.createBuffer({
							length : lenReceived,
							byteOrder : Ti.Codec.LITTLE_ENDIAN
						});

						//dIn.readFully(buffer);
						Ti.API.info("bufferOffset before copy buffer: " + bufferOffset);
						buffer.copy(readBuffer, 0, bufferOffset, lenReceived);
						bufferOffset += lenReceived;

						//recvRecogInfo.add(new RecogInfo(type, buffer));
						recvRecogInfo[i] = {
							type : typeReceived,
							data : buffer
						};
					}
				}

				if (recvRecogInfo.length == 0) {
					Ti.UI.createNotification({
						message : "Server connetion failed",
						duration : Ti.UI.NOTIFICATION_DURATION_SHORT
					}).show();
				} else if (recvRecogInfo[0].type == TYPE_STR) {
					var bufferString = Ti.Codec.decodeString({
						source : recvRecogInfo[0].data
					});
					Ti.API.info("bufferString: " + bufferString);

					if (bufferString == "fail") {
						Ti.UI.createNotification({
							message : "Recognition failed",
							duration : Ti.UI.NOTIFICATION_DURATION_SHORT
						}).show();
					} else {
						var cs = [];

						Ti.API.info("recvRecogInfo.length: " + recvRecogInfo.length);
						for (var i = 0; i < recvRecogInfo.length; i++) {
							cs[i] = Ti.Codec.decodeString({
								source : recvRecogInfo[i].data
							});
							Ti.API.info("cs[" + i + "]: " + cs[i]);
						}

						var opts = {
							title : 'Pick solution',
							options : cs
						};

						var dialog = Ti.UI.createOptionDialog(opts);
						dialog.addEventListener('click', function(e) {
							var which = e.index;
							Ti.API.info("which: " + which + "/" + recvRecogInfo[which]);

							var sendType = TYPE_STR;
							var sendMsg = Ti.Codec.decodeString({
								source : recvRecogInfo[which].data
							});

							readBuffer = Ti.createBuffer();

							var socket2 = Ti.Network.Socket.createTCP({
								host : '125.132.41.25',
								//host : '172.25.235.46',
								port : 27016,
								connected : function(e) {
									Ti.API.info('Socket opened!');

									var h0 = Ti.createBuffer({
										value : 1,
										type : Ti.Codec.TYPE_INT,
										byteOrder : Ti.Codec.LITTLE_ENDIAN
									});

									var h1 = Ti.createBuffer({
										value : TYPE_STR,
										type : Ti.Codec.TYPE_INT,
										byteOrder : Ti.Codec.LITTLE_ENDIAN
									});

									var h2 = Ti.createBuffer({
										value : recvRecogInfo[which].data.length,
										type : Ti.Codec.TYPE_INT,
										byteOrder : Ti.Codec.LITTLE_ENDIAN
									});

									var hId = Ti.createBuffer({
										value : genRandomId(),
										type : Ti.Codec.CHARSET_UTF8,
										byteOrder : Ti.Codec.LITTLE_ENDIAN,
										length : 24
									});

									var data = recvRecogInfo[which].data;

									var bufferIntegrated = Ti.createBuffer();

									bufferIntegrated.append(h0);
									bufferIntegrated.append(h1);
									bufferIntegrated.append(h2);
									bufferIntegrated.append(hId);
									bufferIntegrated.append(data);

									Ti.Stream.pump(socket2, readCallback, 4096, true);

									//Ti.Stream.write(socket2, h0, writeCallback);
									//Ti.Stream.write(socket2, h1, writeCallback);
									//Ti.Stream.write(socket2, dataLength, writeCallback);
									//Ti.Stream.write(socket2, hId, writeCallback);
									Ti.Stream.write(socket2, bufferIntegrated, writeCallback);
								},
								error : function(e) {
									Ti.API.info('Error (' + e.errorCode + '): ' + e.error);
								},
							});
							socket2.connect();

						});
						dialog.show();
					}

				} else if (recvRecogInfo[0].type == TYPE_PIC) {
					var picPaths = [];
					for (var i = 0; i < recvRecogInfo.length; i++) {
						var data = recvRecogInfo[i].data;
						picPaths[i] = saveGallery(data, i, "png");
					}

				}
				e.source.close();
				Ti.API.info("socket closed!");
			} else {
				try {
					if (e.buffer) {
						readBuffer.append(e.buffer);
					} else {
						Ti.API.error('Error: read callback called with no buffer!');
					}
				} catch (ex) {
					Ti.API.error(ex);
				}
			}
		}

		function writeCallback(e) {
			if (e.success) {
				Ti.API.info("Successfully wrote to socket.");
			} else
				Ti.API.error("[" + e.code + "]" + e.error);
		}

	},
	cancel : function() {
	},
	error : function(error) {
		var a = Titanium.UI.createAlertDialog({
			title : 'Camera'
		});
		if (error.code == Titanium.Media.NO_CAMERA) {
			a.setMessage('Please run this test on device');
		} else {
			a.setMessage('Unexpected error: ' + error.code);
		}
		a.show();
	},
	overlay : overlay,
	showControls : false, // don't show system controls
	mediaTypes : Ti.Media.MEDIA_TYPE_PHOTO,
	autohide : false // tell the system not to auto-hide and we'll do it ourself
});

function genRandomId() {
	var textArray = [];
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for (var i = 0; i < 20; i++)
		textArray.push(possible.charAt(Math.floor(Math.random() * possible.length)));

	textArray.push("\0");

	return textArray.join('');
}

function saveGallery(data, nameIdx, ext) {
	/*
	 try {
	 File sdCard = Environment.getExternalStorageDirectory();
	 File dir = new File (sdCard.getAbsolutePath() + "/smartqna/");
	 dir.mkdirs();

	 String fileName;

	 if(nameIdx < 0)	fileName = String.format("%d.%s", System.currentTimeMillis(), ext);
	 else			fileName = String.format("%d.%s", nameIdx, ext);

	 File outFile = new File(dir, fileName);

	 FileOutputStream outStream = new FileOutputStream(outFile);
	 outStream.write(data);
	 outStream.flush();
	 outStream.close();

	 String storePath = outFile.getAbsolutePath();
	 Log.d(TAG, "onPictureTaken - wrote bytes: " + data.length + " to " + storePath);

	 //refreshGallery(outFile);

	 return storePath;
	 } catch(Exception e){
	 e.printStackTrace();
	 return null;
	 }
	 */

	var sdCard = Ti.Filesystem.getExternalStorageDirectory();
	Ti.API.info("sdCard: " + sdCard);
	var dir = Ti.Filesystem.getFile(sdCard, 'smartqna');
	if (!dir.exists())
		dir.createDirectory();

	var fileName;
	if (nameIdx < 0)
		fileName = (new Date().getTime().toString) + "." + ext;
	else
		fileName = nameIdx + "." + ext;

	var outFile = Ti.Filesystem.getFile(dir.resolve(), fileName);
	Ti.API.info("dir.resolve(): " + dir.resolve());
	
	var fileContents = Ti.Codec.decodeString({
		source : data,
	});
	outFile.write(fileContents);

	var storePath = outFile.resolve();
	Ti.API.info("onPictureTaken - wrote bytes: " + fileContents.length + " to " + storePath);

	dir = null;
	outFile = null;

	return storePath;
}
